package buu.isiriporn.additionapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ScoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)

        val sum:String = intent.getStringExtra("sum")?:"0"
        val txtSum = findViewById<TextView>(R.id.txtSum)
        txtSum.text = sum

        val correct:String = intent.getStringExtra("correct")?:"0"
        val txtCorrect = findViewById<TextView>(R.id.txtCorrect)
        txtCorrect.text = correct

        val incorrect:String = intent.getStringExtra("incorrect")?:"0"
        val txtIncorrect = findViewById<TextView>(R.id.txtIncorrect)
        txtIncorrect.text = incorrect
    }
}